package barcode;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Date;

// Image image = SwingFXUtils.toFXImage(capture, null);
public class Barcode {

    private String id;
    private Date date;
    private String value;

    private static final double LINE_HEIGHT = 22.85;
    private static final double CONSTRAINT_LINE_HEIGHT = LINE_HEIGHT + 1.65;
    private static final double DIGIT_HEIGHT = 2.75;
    private static final double DIGIT_MARGIN = 0.165;
    private static final double DEFAULT_LINE_WIDTH = 0.15;
    private static final double ZERO_LINE_WIDTH = 1.35;
    private static final double LINE_SPACING = 0.2;

    private static final double SCALE = 10;


    public Barcode(String id, Date date, String value) {
        this.id = id;
        this.date = date;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Image generateImage() {
        BufferedImage img = new BufferedImage(calcWidth(), 350, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = img.createGraphics();

        g.setColor(Color.WHITE);
        g.fillRect(0, 0, 400, 300);

        g.setColor(Color.BLACK);
        g.setFont(new Font(Font.SERIF, Font.PLAIN, (int) (DIGIT_HEIGHT * SCALE)));
        g.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        double x = 0;
        for (int i = 0; i < value.length(); i++) {
            int c = Integer.parseInt(value.substring(i, i + 1));
            System.out.println(c);
            double width = 0;
            double height = getLineLength(i);
            Color color = Color.BLACK;

            if (c == 0) {
                color = Color.WHITE;
                width = ZERO_LINE_WIDTH;
            } else {
                width = c * DEFAULT_LINE_WIDTH;
            }
            System.out.println(width + " " + height);
            width *= SCALE;
            height *= SCALE;
            g.setColor(color);

            if (color != Color.WHITE) {
                g.fillRect((int) x, 0, (int) width, (int) height);
            }

//            double y = height + (DIGIT_MARGIN * SCALE) + (DIGIT_HEIGHT * SCALE);
//            g.drawString(Integer.toString(c), (int) x, (int) y);

            x += width + (LINE_SPACING * SCALE);
        }

        return cropImage(img, (int) x);
    }

    private BufferedImage cropImage(BufferedImage src, int width) {
        BufferedImage dest = src.getSubimage(0, 0, width, src.getHeight());
        return dest;
    }

    private int calcWidth() {
        return 373; // 37.29 25.93
    }

    private double getLineLength(int i) {
        if ((i == 0) || (i == value.length() - 1) || (i == value.length() / 2)) {
            return CONSTRAINT_LINE_HEIGHT;
        }
        return LINE_HEIGHT;
    }
}
